/**
 * @file
 * uWaterloo Web Resources Customizations js
 *
 * Set of jQuery related routines.
 */
(function ($) {

  Drupal.behaviors.uw_web_resources_customizations = {
    attach: function (context, settings) {
      $('.uw-site--title h1').each(function() {
        if (!$(this).is('[data-uwrc]')) {
          html = $(this).html().replace(/#[0-9A-Fa-f]{6}/g, '$&  <svg width=".8em" height=".8em" style="width: auto;"><rect width="100%" height="100%" fill="$&" /></svg>');
          $(this).html(html);
          $(this).attr('data-uwrc','1');
        }
      });
    }
  }

})(jQuery);
